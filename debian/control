Source: revolt
Section: net
Priority: optional
Maintainer: Matrix Packaging Team <pkg-matrix-maintainers@lists.alioth.debian.org>
Uploaders: Hubert Chathi <uhoreg@debian.org>
Build-Depends: debhelper-compat (= 12), libglib2.0-bin, libglib2.0-dev-bin, python3:any, python3-gi, dh-python
Standards-Version: 4.5.0
Homepage: https://github.com/aperezdc/revolt
Vcs-Git: https://salsa.debian.org/matrix-team/revolt.git
Vcs-Browser: https://salsa.debian.org/matrix-team/revolt

Package: revolt
Architecture: all
Depends: python3:any, python3-gi, gir1.2-webkit2-4.1, ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Description: better desktop integration for Element
 Revolt is a small application which wraps the Element webapp to provide better
 integration with desktop environments in general, and GNOME in particular:
 .
   * Having Element as a "standalone" application with its own window, launcher,
     icon, etc. instead of it living in a browser tab.
   * Persistent notifications (for desktop environments supporting them, i.e.
     GNOME). Notifications are automatically prevented when the Revolt window
     is focused.
   * Status icon for desktop environment which have a tray bar applet (XFCE,
     Budgie, likely many others).
 .
 Element is a chat client for Matrix, an open, federated communications
 protocol.
